﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AudioNetworkTechTest.Communication;
using AudioNetworkTechTest.Models;
using AudioNetworkTechTest.Repository;
using Moq;
using Xunit;

namespace AudioNetworkTechTest.Tests
{
    public class MusicRepositoryTests
    {
        private readonly Mock<IAudioNetworkApiClient> mockClient;
        private readonly MusicRepository repo;
        private readonly IEnumerable<Track> metalAndPopTracks;
        private readonly IEnumerable<Composer> composers;
        private readonly IDictionary<int, Composer> composersLookup;

        public MusicRepositoryTests()
        {            
            mockClient = new Mock<IAudioNetworkApiClient>();
            repo = new MusicRepository(mockClient.Object);

            
            metalAndPopTracks = new List<Track>
            {
                new Track { Id = 1, Title = "The Trooper", ComposerId = 1, Genre = "metal" },
                new Track { Id = 2, Title = "Set Fire To The Rain", ComposerId = 2, Genre = "pop" },
                new Track { Id = 3, Title = "The Wicker Man", ComposerId = 1, Genre = "metal" }
            };

            composersLookup = new Dictionary<int, Composer>
            {                
                { 1, new Composer { Id = 1, FirstName = "Steve", LastName = "Harris", Title = "Mr", Award = "Legend" } },
                { 2, new Composer { Id = 2, FirstName = "Adele", LastName = "", Title = "Ms", Award = "?" } }
            };

            composers = composersLookup.Values;


            mockClient.Setup(c => c.GetTracksAsync()).Returns(() => Task.FromResult(metalAndPopTracks));
            mockClient.Setup(c => c.GetComposersAsync()).Returns(() => Task.FromResult(composers));
        }

        [Fact]
        public async Task Test_Tracks_Populates()
        {
            await repo.PopulateTracks();

            Assert.Equal(metalAndPopTracks.Count(), repo.Tracks.Count);
        }

        [Fact]
        public async Task Test_Composers_Populates()
        {
            await repo.PopulateComposers();

            Assert.Equal(composers.Count(), repo.Composers.Count);
        }

        [Fact]
        public async Task Test_Track_By_Composer_Ordered()
        {
            var orderedTracks = metalAndPopTracks.Where(t => t.Genre == "metal").OrderBy(t => t.Title).ToArray();

            await repo.PopulateTracks();
            await repo.PopulateComposers();

            var result = repo.DisplayTracksByComposerForGenreOrdered("metal");

            Assert.Equal("The Trooper by Steve Harris", result[0]);
            Assert.Equal("The Wicker Man by Steve Harris", result[1]);
        }
    }
}
