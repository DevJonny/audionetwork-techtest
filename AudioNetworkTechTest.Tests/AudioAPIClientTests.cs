using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AudioNetworkTechTest.Communication;
using AudioNetworkTechTest.Models;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace AudioNetworkTechTest.Tests
{
    public class AudioApiClientTests
    {
        private readonly Mock<MockHttpMessageHandler> mockHttpMessageHandler;
        private readonly HttpClient httpClient;
        private readonly IEnumerable<Track> tracks;
        private readonly IEnumerable<Composer> composers;
        private readonly Mock<IAudioNetworkApis> apis;

        public AudioApiClientTests()
        {            
            mockHttpMessageHandler = new Mock<MockHttpMessageHandler> { CallBase = true };
            httpClient = new HttpClient(mockHttpMessageHandler.Object);
            tracks = new List<Track>();
            composers = new List<Composer>();
            apis = new Mock<IAudioNetworkApis>();

            apis.Setup(a => a.ListAllTracks).Returns(@"https://private-anon-0c6d9d1645-audionetworkrecruitment.apiary-mock.com/tracks");
            apis.Setup(a => a.ListAllComposers).Returns(@"https://private-anon-0c6d9d1645-audionetworkrecruitment.apiary-mock.com/composers");
        }

        [Fact]
        public async Task Test_Valid_Track_Request()
        {
            mockHttpMessageHandler.Setup(h => h.Send(It.IsAny<HttpRequestMessage>())).Returns(new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(tracks))
            });            

            var apiClient = new AudioApiClient(httpClient, apis.Object);
            var result = await apiClient.GetTracksAsync();

            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<Track>>(result);
        }

        [Fact]
        public async Task Test_Invalid_Track_Request()
        {
            mockHttpMessageHandler.Setup(h => h.Send(It.IsAny<HttpRequestMessage>())).Returns(new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.NotFound,
                Content = new StringContent("API doesn't exist!")
            });

            var apiClient = new AudioApiClient(httpClient, apis.Object);
            
            await Assert.ThrowsAsync<HttpRequestException>(() => apiClient.GetTracksAsync());
        }

        [Fact]
        public async Task Test_Valid_Composer_Request()
        {
            mockHttpMessageHandler.Setup(h => h.Send(It.IsAny<HttpRequestMessage>())).Returns(new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(composers))
            });            

            var apiClient = new AudioApiClient(httpClient, apis.Object);
            var result = await apiClient.GetComposersAsync();

            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<Composer>>(result);
        }

        [Fact]
        public async Task Test_Invalid_Composer_Request()
        {
            mockHttpMessageHandler.Setup(h => h.Send(It.IsAny<HttpRequestMessage>())).Returns(new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.NotFound,
                Content = new StringContent("API doesn't exist!")
            });

            var apiClient = new AudioApiClient(httpClient, apis.Object);

            await Assert.ThrowsAsync<HttpRequestException>(() => apiClient.GetComposersAsync());
        }
    }
}
