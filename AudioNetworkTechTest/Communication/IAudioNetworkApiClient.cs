﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AudioNetworkTechTest.Models;

namespace AudioNetworkTechTest.Communication
{
    public interface IAudioNetworkApiClient
    {
        Task<IEnumerable<Track>> GetTracksAsync();
        Task<IEnumerable<Composer>> GetComposersAsync();
    }
}
