﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using AudioNetworkTechTest.Models;
using Newtonsoft.Json;

namespace AudioNetworkTechTest.Communication
{
    public class AudioApiClient : IAudioNetworkApiClient
    {
        private readonly HttpClient httpClient;
        private readonly IAudioNetworkApis apis;

        public AudioApiClient(HttpClient httpClient, IAudioNetworkApis apis)
        {
            this.httpClient = httpClient;
            this.apis = apis;
        }

        public async Task<IEnumerable<Track>> GetTracksAsync()
        {
            var json = await httpClient.GetStringAsync(apis.ListAllTracks);

            return JsonConvert.DeserializeObject<IEnumerable<Track>>(json);
        }

        public async Task<IEnumerable<Composer>> GetComposersAsync()
        {
            var json = await httpClient.GetStringAsync(apis.ListAllComposers);

            return JsonConvert.DeserializeObject<IEnumerable<Composer>>(json);
        }
    }
}
