﻿namespace AudioNetworkTechTest.Communication
{
    public interface IAudioNetworkApis
    {
        string ListAllTracks { get; }
        string ListAllComposers { get; }
    }
}
