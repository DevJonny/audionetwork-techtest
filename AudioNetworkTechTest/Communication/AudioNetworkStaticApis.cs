﻿namespace AudioNetworkTechTest.Communication
{
    public class AudioNetworkStaticApis : IAudioNetworkApis
    {
        public string ListAllTracks => @"https://private-anon-0c6d9d1645-audionetworkrecruitment.apiary-mock.com/tracks";
        public string ListAllComposers => @"https://private-anon-0c6d9d1645-audionetworkrecruitment.apiary-mock.com/composers";
    }
}
