﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AudioNetworkTechTest.Communication;
using AudioNetworkTechTest.Models;

namespace AudioNetworkTechTest.Repository
{
    public class MusicRepository
    {
        public List<Track> Tracks { get; set; } = new List<Track>();
        public IDictionary<int, Composer> Composers { get; set; } = new Dictionary<int, Composer>();

        private readonly IAudioNetworkApiClient apiClient;

        public MusicRepository(IAudioNetworkApiClient apiClient)
        {
            this.apiClient = apiClient;
        }

        public async Task PopulateTracks()
        {
            Tracks.AddRange(await apiClient.GetTracksAsync());
        }

        public async Task PopulateComposers()
        {
            var composers = await apiClient.GetComposersAsync();

            Composers = composers.ToDictionary(c => c.Id, c => c);
        }

        public string[] DisplayTracksByComposerForGenreOrdered(string genre)
        {
            var display = new List<string>();

            foreach (var track in Tracks.Where(t => t.Genre == genre).OrderBy(t => t.Title))
            {
                var composer = Composers[track.ComposerId];

                display.Add($"{track.Title} by {composer.FirstName} {composer.LastName}");
            }

            return display.ToArray();
        }
    }
}
