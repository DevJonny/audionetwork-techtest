﻿using System;
using System.Net.Http;
using AudioNetworkTechTest.Communication;
using AudioNetworkTechTest.Repository;

namespace AudioNetworkTechTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var apis = new AudioNetworkStaticApis();
            var client = new HttpClient();
            var apiClient = new AudioApiClient(client, apis);
            var repo = new MusicRepository(apiClient);

            repo.PopulateTracks().Wait();
            repo.PopulateComposers().Wait();

            var rockTracks = repo.DisplayTracksByComposerForGenreOrdered("rock");

            foreach (var rockTrack in rockTracks)
            {
                Console.WriteLine(rockTrack);
            }

            Console.ReadLine();
        }
    }
}
